from unittest import mock

from rest_framework.test import APITestCase

from services.clients.twitter import TwitterClient


class TestTwitter(APITestCase):
    def test_client__username(self):
        c = TwitterClient()
        l = c.search_by_username('davidwang0524')
        self.assertTrue(len(l) >= 2)

    def test_client__hashtag(self):
        c = TwitterClient()
        l = c.search_by_hashtag('ifttt')
        self.assertTrue(len(l) >= 2)

    def test_client__post(self):
        c = TwitterClient()
        # l = c.post_to_davids_twitter('hello world test')
        # self.assertTrue(l)

    def test_transform(self):
        d = {'created_at': 'Fri Feb 23 04:57:12 +0000 2018',
             'hashtags': [],
             'id': 966899721557049344,
             'id_str': '966899721557049344',
             'lang': 'en',
             'source': '<a href="http://www.ifttt.com" '
                       'rel="nofollow">davidwang0524</a>',
             'text': 'hello world!!',
             'urls': [],
             'user': {'created_at': 'Thu Jan 17 02:00:49 +0000 2013',
                      'default_profile': True,
                      'default_profile_image': True,
                      'followers_count': 2,
                      'friends_count': 3,
                      'id': 1096917217,
                      'id_str': '1096917217',
                      'lang': 'en',
                      'name': 'David Wang',
                      'profile_background_color': 'C0DEED',
                      'profile_link_color': '1DA1F2',
                      'profile_sidebar_border_color': 'C0DEED',
                      'profile_sidebar_fill_color': 'DDEEF6',
                      'profile_text_color': '333333',
                      'profile_use_background_image': True,
                      'screen_name': 'davidwang0524',
                      'statuses_count': 4},
             'user_mentions': []}
        obj = mock.Mock(**d)
        c = TwitterClient()
        result = c.transform(obj)
        expected = {'text': 'hello world!!',
                    'created_at': '2018-02-23T04:57:12+00:00',
                    'meta': {'id': '966899721557049344',
                             'timestamp': 1519361832}}
        self.assertTrue(result == expected)
