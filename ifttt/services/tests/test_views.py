from django.urls import reverse
from rest_framework.status import HTTP_401_UNAUTHORIZED, HTTP_200_OK, \
    HTTP_400_BAD_REQUEST
from rest_framework.test import APITestCase

from services.views import SERVICE_KEY


class TestAllViews(APITestCase):
    def test_invalid_channel_key(self):
        views = [('status_view', self.client.get),
                 ('setup_view', self.client.post),
                 ('by_user_view', self.client.post),
                 ('tweet_view', self.client.post)]
        for view, method in views:
            # no channel key
            url = reverse(view)
            expected_data = {'errors': [{'message': 'invalid chan key'}]}
            response = method(url, {}, format='json')
            assert response.status_code == HTTP_401_UNAUTHORIZED
            assert response.data == expected_data

            # incorrect channel key
            self.client._credentials.update({'HTTP_IFTTT_CHANNEL_KEY': '123'})
            response = method(url, {}, format='json')
            assert response.status_code == HTTP_401_UNAUTHORIZED
            assert response.data == expected_data


class TestStatusSetupViews(APITestCase):
    def test_status_view(self):
        url = reverse('status_view')
        self.client._credentials.update(
            {'HTTP_IFTTT_CHANNEL_KEY': SERVICE_KEY})
        response = self.client.get(url, {}, format='json')
        assert response.status_code == HTTP_200_OK

    def test_setup_view(self):
        url = reverse('setup_view')
        self.client._credentials.update(
            {'HTTP_IFTTT_CHANNEL_KEY': SERVICE_KEY})
        response = self.client.post(url, {}, format='json')
        assert response.status_code == HTTP_200_OK
        assert response.data == {
            'data': {'samples': {
                'triggers': {'by_user': {'username': 'davidwang0524'}},
                'actions': {'tweet': {'message': 'hello world!'}}}}}


class TestByUserView(APITestCase):
    def test_no_username(self):
        url = reverse('by_user_view')
        self.client._credentials.update(
            {'HTTP_IFTTT_CHANNEL_KEY': SERVICE_KEY,
             'HTTP_IFTTT_TEST_MODE': '1'})
        data = {'triggerFields': {'not_username': 'name'}}
        response = self.client.post(url, data, format='json')
        assert response.status_code == HTTP_400_BAD_REQUEST
        assert response.data == {'errors': [{'message': 'invalid username'}]}

    def test_view(self):
        url = reverse('by_user_view')
        self.client._credentials.update(
            {'HTTP_IFTTT_CHANNEL_KEY': SERVICE_KEY,
             'HTTP_IFTTT_TEST_MODE': '1'})
        data = {'triggerFields': {'username': 'name'}}
        response = self.client.post(url, data, format='json')
        assert response.status_code == HTTP_200_OK
        assert response.data == {'data': [
            {'created_at': '2018-02-23T04:57:12+00:00',
             'meta': {'id': '966899721557049344', 'timestamp': 1519361832},
             'text': 'hello world!!'},
            {'created_at': '2018-02-23T00:15:44+00:00',
             'meta': {'id': '966828888021069824', 'timestamp': 1519344944},
             'text': 'hello world # 2'},
            {'created_at': '2018-02-23T00:11:54+00:00',
             'meta': {'id': '966827922089750528', 'timestamp': 1519344714},
             'text': 'hello world!'}]}

    def test_with_limit(self):
        url = reverse('by_user_view')
        self.client._credentials.update(
            {'HTTP_IFTTT_CHANNEL_KEY': SERVICE_KEY,
             'HTTP_IFTTT_TEST_MODE': '1'})
        data = {'triggerFields': {'username': 'name'},
                'limit': 1}
        response = self.client.post(url, data, format='json')
        assert response.status_code == HTTP_200_OK
        assert len(response.data['data']) == 1

        data = {'triggerFields': {'username': 'name'},
                'limit': 0}
        response = self.client.post(url, data, format='json')
        assert response.status_code == HTTP_200_OK
        assert len(response.data['data']) == 0


class TestTweetView(APITestCase):
    def test_no_message(self):
        url = reverse('tweet_view')
        self.client._credentials.update(
            {'HTTP_IFTTT_CHANNEL_KEY': SERVICE_KEY,
             'HTTP_IFTTT_TEST_MODE': '1'})
        data = {'actionFields': {'not_message': 'some message'}}
        response = self.client.post(url, data, format='json')
        assert response.status_code == HTTP_400_BAD_REQUEST
        assert response.data == {'errors': [{'message': 'invalid message'}]}

    def test_view(self):
        url = reverse('tweet_view')
        self.client._credentials.update(
            {'HTTP_IFTTT_CHANNEL_KEY': SERVICE_KEY,
             'HTTP_IFTTT_TEST_MODE': '1'})
        data = {'actionFields': {'message': 'some message'}}
        response = self.client.post(url, data, format='json')
        assert response.status_code == HTTP_200_OK
        assert response.data == {'data': [
            {'id': '966899721557049344',
             'url': 'https://twitter.com/davidwang0524/'
                    'status/966899721557049344'}]}
