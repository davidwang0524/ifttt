from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_401_UNAUTHORIZED, \
    HTTP_400_BAD_REQUEST

from services.clients.twitter import TwitterClient, twitter_mock

SERVICE_KEY = '0Mm6cPN4sEqSjdbyPoffefMFtimE3vsblseF_Hzua34fVRoBEZycoCLmI7byu2bX'
twitter_client = TwitterClient()


class UTF8Renderer(JSONRenderer):
    charset = 'utf-8'


@api_view(['GET'])
@renderer_classes((UTF8Renderer,))
def status_view(request):
    if not _is_valid_channel_key(request):
        return _get_error_response('invalid chan key', HTTP_401_UNAUTHORIZED)
    return Response(status=HTTP_200_OK)


@api_view(['POST'])
@renderer_classes((UTF8Renderer,))
def setup_view(request):
    if not _is_valid_channel_key(request):
        return _get_error_response('invalid chan key', HTTP_401_UNAUTHORIZED)
    body = {
        'data': {
            'samples': {
                'triggers': {
                    'by_user': {
                        'username': 'davidwang0524',
                    }
                },
                'actions': {
                    'tweet': {
                        'message': 'hello world!'
                    }
                },
            }
        }
    }
    return Response(body, status=HTTP_200_OK)


@api_view(['POST'])
@renderer_classes((UTF8Renderer,))
def by_user_view(request):
    if not _is_valid_channel_key(request):
        return _get_error_response('invalid chan key', HTTP_401_UNAUTHORIZED)

    username = request.data.get('triggerFields', {}).get('username')
    limit = int(request.data.get('limit', -1))
    if not username:
        return _get_error_response('invalid username', HTTP_400_BAD_REQUEST)

    twitter = twitter_mock if _is_test(request) else twitter_client
    data = twitter.search_by_username(username)
    if limit >= 0:
        data = data[:limit]
    body = {'data': data}
    return Response(body, status=HTTP_200_OK)


@api_view(['POST'])
@renderer_classes((UTF8Renderer,))
def tweet_view(request):
    if not _is_valid_channel_key(request):
        return _get_error_response('invalid chan key', HTTP_401_UNAUTHORIZED)

    message = request.data.get('actionFields', {}).get('message')
    if not message:
        return _get_error_response('invalid message', HTTP_400_BAD_REQUEST)

    twitter = twitter_mock if _is_test(request) else twitter_client
    data = twitter.post_to_davids_twitter(message)
    body = {'data': data}
    return Response(body, status=HTTP_200_OK)


def _is_test(request):
    test_mode = request.META.get('HTTP_IFTTT_TEST_MODE')
    return test_mode == '1'


def _get_error_response(message, status):
    error_body = {'errors': [{'message': message}]}
    return Response(error_body, status=status)


def _is_valid_channel_key(request):
    channel_key = request.META.get('HTTP_IFTTT_CHANNEL_KEY')
    return channel_key == SERVICE_KEY
