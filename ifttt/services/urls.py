from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from services import views

urlpatterns = [
    url(r'status', views.status_view, name='status_view'),
    url(r'test/setup', views.setup_view, name='setup_view'),
    url(r'triggers/by_user', views.by_user_view, name='by_user_view'),
    url(r'actions/tweet', views.tweet_view, name='tweet_view'),
]
urlpatterns = format_suffix_patterns(urlpatterns)
