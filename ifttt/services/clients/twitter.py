from datetime import datetime, timezone
from unittest import mock

import twitter
from dateutil.parser import parse

consumer_key = 'IeKfuA73NWjsEgloK5vlrPwW9'
consumer_secret = 'PIdFUk1UAujMPtmeORZzEV7PSkwD2pcOaWzHqwQRYydZUe7Qd8'
access_token = '1096917217-3bUZ2yk7SeQmnYZU5fdCw817Oa7GXXvDbGR2mIk'
access_token_secret = 'aiZ4365mCkF47FnFkALwKsjEiWPPBejopZMuDfquBp09v'

api = twitter.Api(consumer_key=consumer_key,
                  consumer_secret=consumer_secret,
                  access_token_key=access_token,
                  access_token_secret=access_token_secret)


class TwitterClient:
    query_suffix = '&count=15&result_type=recent&lang=en'

    def search_by_username(self, username):
        query = 'q=from%3A{}{}'.format(username, self.query_suffix)
        results = api.GetSearch(raw_query=query)
        return [self.transform(result) for result in results]

    def search_by_hashtag(self, hashtag):
        query = 'q=%23{}{}'.format(hashtag, self.query_suffix)
        results = api.GetSearch(raw_query=query)
        return [result.text for result in results]

    def post_to_davids_twitter(self, msg):
        result = api.PostUpdate(msg)
        # id and url
        return [{
            'id': result.id_str,
            'url': 'https://twitter.com/davidwang0524/status/{}'.format(
                result.id_str)
        }]

    def transform(self, result):
        """
        [
    {
      "CreatedAt": "2013-11-04T09:23:00-07:00"
      "meta": {
        "id": "14b9-1fd2-acaa-5df5",
        "timestamp": 1383597267
      }
    },
    {
      "CreatedAt": "2013-11-04T03:23:00-07:00"
      "meta": {
        "id": "ffb27-a63e-18e0-18ad",
        "timestamp": 1383596355
      }
    }
  ]
  {'created_at': 'Fri Feb 23 04:57:12 +0000 2018',
             'hashtags': [],
             'id': 966899721557049344,
             'id_str': '966899721557049344',
             'lang': 'en',
             'source': '<a href="http://www.ifttt.com" '
                       'rel="nofollow">davidwang0524</a>',
             'text': 'hello world!!',
             'urls': [],
             'user_mentions': []}
        """
        dt = parse(result.created_at)
        delta = dt - datetime(1970, 1, 1, tzinfo=timezone.utc)
        timestamp = int(delta.total_seconds())  # total seconds is a float
        created_at = dt.isoformat()
        return {
            'text': result.text,
            'created_at': created_at,
            'meta': {
                'id': result.id_str,
                'timestamp': timestamp,
            }
        }


twitter_mock = mock.Mock(
    search_by_username=mock.Mock(return_value=[
        {'text': 'hello world!!', 'created_at': '2018-02-23T04:57:12+00:00',
         'meta': {'id': '966899721557049344', 'timestamp': 1519361832}},
        {'text': 'hello world # 2', 'created_at': '2018-02-23T00:15:44+00:00',
         'meta': {'id': '966828888021069824', 'timestamp': 1519344944}},
        {'text': 'hello world!', 'created_at': '2018-02-23T00:11:54+00:00',
         'meta': {'id': '966827922089750528', 'timestamp': 1519344714}}]),
    post_to_davids_twitter=mock.Mock(return_value=[
        {'id': '966899721557049344',
         'url': 'https://twitter.com/davidwang0524/status/'
                '966899721557049344'}]),
)
